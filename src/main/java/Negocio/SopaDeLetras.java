package Negocio;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class SopaDeLetras {

    private char sopas[][];
    private ArrayList<Posicion> posiciones;

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {
        posiciones = new ArrayList();
    }

    @Override
    public String toString() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "      ";
            }
            msg += "\n";
        }
        return msg;
    }

    /**
     * Lee el excel buscado y retorna la matriz que contiene la primera hoja si
     * la matriz es dispersa lansa una excepcion
     */
    private static char[][] leerExcel(File fichero) throws Exception {

        char[][] matrizExcel;

        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(fichero));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;

        HSSFRow fila1 = hoja.getRow(0);
        int tamanioHorizontal = (fila1.getLastCellNum());

        matrizExcel = new char[canFilas][tamanioHorizontal + 1];

        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol = filas.getLastCellNum();

            if (tamanioHorizontal == cantCol) {
                for (int j = 0; j < cantCol; j++) {
                    //Obtiene la celda y su valor respectivo
                    //double r=filas.getCell(i).getNumericCellValue();
                    String valor = filas.getCell(j).getStringCellValue();
                    matrizExcel[i][j] = valor.charAt(0);
                }
            } else {
                throw new Exception("Materiz dispersa");
            }
        }
        return matrizExcel;
    }

    /**
     * Llena el atributo matriz con la sopa de letras leida del exce
     *
     * @param fichero
     * @throws java.lang.Exception
     */
    public void llenarMatriz(File fichero) throws Exception {
        this.sopas = new char[leerExcel(fichero).length][leerExcel(fichero)[0].length];
        for (int i = 0; i < leerExcel(fichero).length; i++) {
            System.arraycopy(leerExcel(fichero)[i], 0, sopas[i], 0, leerExcel(fichero)[i].length);
        }
    }

    /**
     * descompone la palabra buscada en un arreglo de caracteres verifica si la
     * palabra es diferente leyendola de derecha a izquierda y viceversa y manda
     * a buscar la palabra descompuesta
     *
     * @param palabraCadena
     * @throws java.lang.Exception
     */
    public void buscarPalabras(String palabraCadena) throws Exception {
        char[] palabra = new char[palabraCadena.length()];
        for (int i = 0; i < palabraCadena.length(); i++) {
            palabra[i] = palabraCadena.charAt(i);
        }
        buscar(palabra);
        if (!palindromo(palabraCadena)) {
            buscar(invertir(palabra));
        }

    }

    /**
     * Manda a buscar la palabra dividida en multiples direcciones si la palabra
     * no cabe en la sopa lanza una excepcio
     *
     * @param palabran
     * @throws java.lang.Exception
     */
    public void buscar(char[] palabra) throws Exception {
        if(palabra.length == 1)
        {
            buscarHorizontal(palabra);
            return;
        }
        if (sopas[0].length >= palabra.length) {
            buscarHorizontal(palabra);
        }
        if (sopas.length >= palabra.length) {
            buscarVertical(palabra);
        }
        if (sopas.length >= palabra.length) {
            buscarDiagonalDer(palabra);
        }
        if (sopas.length >= palabra.length) {
            buscarDiagonalIzq(palabra);
        }
        if (palabra.length >= sopas.length && palabra.length >= sopas[0].length) {
            throw new Exception("La palabra que busca no cabe en la sopa");
        }
    }

    /**
     * Invierte los arreglos de caractere
     *
     * @param palabras
     * @return
     */
    public char[] invertir(char[] palabra) {
        char[] invertido = new char[palabra.length];
        int maximo = palabra.length;
        for (int i = 0; i < palabra.length; i++) {
            invertido[maximo - 1] = palabra[i];
            maximo--;
        }
        return invertido;
    }

    public void buscarDiagonalIzq(char[] palabra) {
        int contador = 0;
        int posX = 0, posY = 0;
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[0].length; j++) {
                if (i < sopas.length && contador < palabra.length && sopas[i][j] == palabra[contador] ) {
                    if (contador == 0) {
                        posY = i;
                        posX = j;
                    }
                    if (contador == palabra.length - 1) {
                        Posicion p = new Posicion(posX, true, palabra.length);
                        p.setY(posY);
                        p.setDireccion("DIAGIZQ");
                        posiciones.add(p);
                        break;
                    }
                    i++;
                    contador++;
                } else if (contador > 0) {
                    contador = 0;
                    i = posY;
                    j = posX;
                }
            }
        }
    }
    
    public void buscarDiagonalDer(char[] palabra) {
        int contador = 0;
        int posX = 0, posY = 0;
        for (int i = sopas.length-1; i >= 0; i--)
        {
            for (int j = sopas[0].length-1; j >= 0; j--)
            {
                if(contador < palabra.length && i < sopas.length && sopas[i][j]==palabra[contador])
                {
                    if (contador == 0)
                    {
                        posX = j; posY = i;
                    }
                    if (contador == palabra.length - 1) {
                        Posicion p = new Posicion(posX, true, palabra.length);
                        p.setY(posY);
                        p.setDireccion("DIAGDER");
                        posiciones.add(p);
                        break;
                    }
                    i++;
                    contador++;
                }else if (contador > 0) {
                    contador = 0;
                    i = posY;
                    j = posX;
                }
            }
        }
    }

    /**
     * Busca las palabras de forma horizontal si encuentra alguna manda su
     * posicion a una list
     *
     * @param palabra
     */
    public void buscarHorizontal(char[] palabra) {
        for (int i = 0; i < sopas.length; i++) {
            if (buscarEnVector(sopas[i], palabra) != null) {
                Posicion p;
                p = buscarEnVector(sopas[i], palabra);
                p.setY(i);
                p.setDireccion("HORIZONTAL");
                posiciones.add(p);
            }
        }
    }

    /**
     * Busca las palabras de forma vertical si encuentra alguna manda su
     * posicion a una list
     *
     * @param palabraa
     */
    public void buscarVertical(char[] palabra) {
        Posicion p;
        for (int i = 0; i < sopas[0].length; i++) {
            char[] vertical = new char[sopas.length];
            for (int j = 0; j < sopas.length; j++) {
                vertical[j] = sopas[j][i];
            }
            if (buscarEnVector(vertical, palabra) != null) {
                p = buscarEnVector(vertical, palabra);
                p.setY(p.getX());
                p.setX(i);
                p.setDireccion("VERTICAL");
                posiciones.add(p);
            }
        }
    }

    /**
     * Busca si las palabras descompuestas se encuentran en vectores Se puede
     * usar para cualquier sentido de busqueda
     *
     * @param sopaSlidea
     * @param palabra
     * @return
     */
    public Posicion buscarEnVector(char[] vector, char[] palabra) {
        int contador = 0;
        int posicion = 0;
        boolean estado = false;
        for (int i = 0; i < vector.length && contador < palabra.length; i++) {
            if (vector[i] == palabra[contador]) {
                if (contador == 0) {
                    posicion = i;
                }
                contador++;
            } else if (contador > 0) {
                contador = 0;
                i--;
            } else {
                contador = 0;
            }
        }
        if (contador == palabra.length) {
            estado = true;
            return new Posicion(posicion, estado, palabra.length);
        }
        return null;
    }

    /**
     * Verifica si una palabra se lee igual al derecho y al revez Ejemplo oro
     */
    static boolean palindromo(String palabra) {
        String modificado = palabra.replaceAll(" ", "").toLowerCase();
        int i = 0;
        while (i < modificado.length() / 2) {
            if (modificado.charAt(i) != modificado.charAt(modificado.length() - 1 - i)) {
                return false;
            }
            i++;
        }
        return true;
    }

    /**
     * Imprime las posiciones guardadas en la lista Para testear
     */
    public String imprimirPosiciones() {
        String cadena = "";
        for (int i = 0; i < posiciones.size(); i++) {
            cadena += posiciones.get(i).toString() + "\n";
        }
        cadena += "_________________________";
        return cadena;
    }
    /*
        retorna cuantas veces esta la palabra en la matriz
     */

    public char[][] getSopas() {
        return sopas;
    }

    public void setSopas(char[][] sopas) {
        this.sopas = sopas;
    }

    public ArrayList<Posicion> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(ArrayList<Posicion> posiciones) {
        this.posiciones = posiciones;
    }
    
}
