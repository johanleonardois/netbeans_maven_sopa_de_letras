package Negocio;

public class Posicion {
    Integer X;
    Integer Y;
    Boolean find;
    Integer size;
    String direccion;

    public Posicion() {
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Posicion(Integer X, Boolean find, Integer size) {
        this.X = X;
        this.find = find;
        this.size = size;
    }
    
    public Posicion(Boolean find, Integer Y, Integer size) {
        this.find = find;
        this.Y = Y;
        this.size = size;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
    
    
    
    public Integer getX() {
        return X;
    }

    public void setX(Integer X) {
        this.X = X;
    }

    public Integer getY() {
        return Y;
    }

    public void setY(Integer Y) {
        this.Y = Y;
    }

    public Boolean getFind() {
        return find;
    }

    public void setFind(Boolean find) {
        this.find = find;
    }
    
    @Override
    public String toString()
    {
        Integer X2 = X+1;
        Integer Y2 = Y+1;
        String cadena = "X= " + X2.toString() + " Y= " + Y2.toString() + " " + " Tamaño= " + size.toString()+ " Direccion= " + direccion;
        return cadena;
    }
}

