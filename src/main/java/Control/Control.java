package Control;

import Negocio.SopaDeLetras;
import Vista.ImprimirEnPDF;
import Vista.InterfazGrafica;
import com.itextpdf.text.DocumentException;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Control implements ActionListener {

    SopaDeLetras sopa;
    InterfazGrafica gui;

    public static void main(String[] args) {
        SopaDeLetras sopa = new SopaDeLetras();
        InterfazGrafica gui = new InterfazGrafica();
        Control c = new Control(sopa, gui);
        gui.setControlador(c);
        gui.arranca();
    }

    public Control(SopaDeLetras sopa, InterfazGrafica gui) {
        this.sopa = sopa;
        this.gui = gui;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "BuscarA":
                JFileChooser fc = new JFileChooser();
                FileNameExtensionFilter filtroXls = new FileNameExtensionFilter("Archivos Excel 97-2003 (xls)", "xls");
                fc.setFileFilter(filtroXls);
                int seleccion = fc.showOpenDialog(gui);
                if (seleccion == JFileChooser.APPROVE_OPTION) {
                    File fichero = fc.getSelectedFile();
                    gui.fieldDireccion.setText(fichero.getAbsolutePath());
                    try {
                        sopa.llenarMatriz(fichero);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex, "Error de Lectura", 0);
                        gui.fieldDireccion.setText("");
                        return;
                    }
                        Font f = new Font("TimesRoman", Font.BOLD, 20);
                        gui.TextArea.setFont(f);
                        gui.TextArea.setText(sopa.toString());
                }
                break;
            case "BuscarP":
                if (gui.barraDeBusqueda.getText().indexOf(' ') != -1) {
                    JOptionPane.showMessageDialog(null, "Digite la palabra sin espacios");
                    gui.barraDeBusqueda.setText("");
                } else {
                    try {
                        sopa.buscarPalabras(gui.barraDeBusqueda.getText());
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex, "Error", 0);
                    }
                    gui.barraDeBusqueda.setText("");
                }
                break;
            case "Imprimir":
                System.out.println(sopa.imprimirPosiciones());
                ImprimirEnPDF imprimirTabla = new ImprimirEnPDF(sopa.getSopas(), sopa.getPosiciones());
                 {
                    try {
                        imprimirTabla.imprimirPdf();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Control.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (DocumentException ex) {
                        Logger.getLogger(Control.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
            default:
                break;
        }
    }
}
