package Vista;

import Negocio.Posicion;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Font;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ImprimirEnPDF {

    private char[][] sopa;
    private ArrayList<Posicion> posiciones;

    public ImprimirEnPDF() {
    }

    public ImprimirEnPDF(char[][] sopa, ArrayList<Posicion> posiciones) {
        this.sopa = sopa;
        this.posiciones = posiciones;
    }

    public void imprimirPdf() throws FileNotFoundException, DocumentException {
        Document documento = new Document();
        FileOutputStream archivoPdf = new FileOutputStream("SopaResuelta.pdf");
        PdfWriter.getInstance(documento, archivoPdf);
        PdfPTable tabla = new PdfPTable(sopa[0].length);
        for (int i = 0; i < sopa.length; i++) {
            for (int j = 0; j < sopa[0].length; j++) {
                tabla.addCell(casillas()[i][j]);
            }
        }
        documento.open();
        Paragraph titulo = new Paragraph("Sopa De Letras\n\n", FontFactory.getFont("arial", 32, Font.BOLD, BaseColor.RED));
        titulo.setAlignment(2);
        documento.add(titulo);
        documento.add(tabla);
        documento.close();
    }

    public PdfPCell[][] casillas() {
        PdfPCell[][] celdas = new PdfPCell[sopa.length][sopa[0].length];
        for (int i = 0; i < sopa.length; i++) {
            for (int j = 0; j < sopa[0].length; j++) {
                Paragraph columna = new Paragraph();
                com.itextpdf.text.Font fuente = FontFactory.getFont("arial", 24, Font.BOLD, BaseColor.BLACK);
                columna.setAlignment(2);
                columna.setFont(fuente);
                columna.add("" + sopa[i][j]);
                if (palabrasMarcadas()[i][j] != null && palabrasMarcadas()[i][j]) {
                    fuente.setColor(BaseColor.RED);
                    fuente.setStyle(Font.BOLD);
                    columna.setFont(fuente);
                }
                celdas[i][j] = new PdfPCell(columna);
                celdas[i][j].setBorder(Rectangle.NO_BORDER);
            }
        }
        return celdas;
    }

    public Boolean[][] palabrasMarcadas() {
        Boolean[][] marcadas = new Boolean[sopa.length][sopa[0].length];
        
        for (int i = 0; i < sopa.length; i++) {
            for (int j = 0; j < sopa[0].length; j++) {
                for (int k = 0; k < posiciones.size(); k++){
                if(i == posiciones.get(k).getY() && j == posiciones.get(k).getX())
                {
                    int posI = i, posJ = j;
                    if(posiciones.get(k).getDireccion()=="HORIZONTAL")
                    {
                        marcadas[i][j] = true;
                        for(int h = 0; h < posiciones.get(k).getSize(); h++)
                        {
                            marcadas[i][j++] = true;
                        }
                        i=posI;
                        j=posJ;
                    }
                    if(posiciones.get(k).getDireccion() == "VERTICAL")
                    {
                        marcadas[i][j] = true;
                        for(int h = 0; h < posiciones.get(k).getSize(); h++)
                        {
                            marcadas[i++][j] = true;
                        }
                        i=posI;
                        j=posJ;
                    }
                    if(posiciones.get(k).getDireccion() == "DIAGDER")
                    {
                        marcadas[i][j] = true;
                        for(int h = 0; h < posiciones.get(k).getSize(); h++)
                        {
                            marcadas[i++][j--] = true;
                        }
                        i=posI;
                        j=posJ;
                    }
                    if(posiciones.get(k).getDireccion() == "DIAGIZQ")
                    {
                        marcadas[i][j] = true;
                        for(int h = 0; h < posiciones.get(k).getSize(); h++)
                        {
                            marcadas[i++][j++] = true;
                        }
                        i=posI;
                        j=posJ;
                    }
                }
                }
            }
        }
        
        return marcadas;
    }

   
}
